#include "InputException.h"

InputException::InputException(unsigned short int type): errorType(type)
{
	if (type > MULTI_SHAPE)
	{
		throw(std::string("Bad input failed the program"));
	}
}

unsigned short int InputException::getError() const
{
	return errorType;
}
