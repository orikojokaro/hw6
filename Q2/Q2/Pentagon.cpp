#include "Pentagon.h"



void Pentagon::draw()
{
	std::cout << "Side length: " << _side << std::endl << "Area: " << getArea() << std::endl;
}

double Pentagon::getArea() const
{
	return MathUtils::CalPentagonArea(_side);
}

Pentagon::Pentagon(std::string name, double side): Shape(name, std::string("Pentagon")), _side(side)
{
}


Pentagon::~Pentagon()
{
}
