#pragma once
#include <exception>
#include <iostream>
enum {
	BAD_TYPE,
	MULTI_SHAPE
};
class InputException : public std::exception
{
	unsigned short int errorType;

public:
	//no pointer, no need for = or dtor
	InputException(unsigned short int errorType);
	virtual const char* what() const override
	{
		if (BAD_TYPE == errorType)
		{
			return "Enter the correct input type\n";
		}
		else
		{
			return "Warning - Don't try to build more than one shape at once\n";
		}
	}
	unsigned short int getError() const;
};

