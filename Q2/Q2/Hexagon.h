#pragma once
#include <iostream>
#include "MathUtils.h"
#include "Shape.h"
#include "shapeexception.h"
class Hexagon: public Shape
{
	double _side;
public:
	//no pointers, no need for =
	void draw();
	double getArea() const;
	Hexagon(std::string name, double side);
	~Hexagon();
	void setSide(double side)
	{
		if (side < 0)
		{
			throw new shapeException;
		}
		_side = side;
	}
};

