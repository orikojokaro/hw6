#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include "InputException.h"
#include <string>
#include "shapeException.h"
#include "Pentagon.h"
#include "Hexagon.h"
#define ONE_CHAR 1

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180, side = 0; int height = 0, width = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Pentagon pen(nam, side);
	Hexagon hex(nam, side);


	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	std::string temp = "";
	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; char shapetype;
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, P (capital) = Pentagon, H(capital) = Hexagon" << std::endl;
		std::cin >> temp;
		shapetype = temp[0];
		try
		{
			if (temp.length() > ONE_CHAR)
			{
				throw new InputException(MULTI_SHAPE);
			}
			switch (shapetype) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				try
				{
					std::cin >> nam >> col >> height >> width >> ang >> ang2;
					para.setName(nam);
					para.setColor(col);
					para.setHeight(height);
					para.setWidth(width);
					para.setAngle(ang, ang2);
					ptrpara->draw();
				}
				catch(shapeException * e)
				{
					std::cout << e->what() << std::endl;
				}
				break;
			case 'P':
				std::cout << "enter name, color, side len" << std::endl;
				std::cin >> nam >> col >> side;
				pen.setName(nam);
				pen.setColor(col);
				pen.setSide(side);
				pen.draw();
				break;
			case 'H':
				std::cout << "enter name, color, side len" << std::endl;
				std::cin >> nam >> col >> side;
				hex.setName(nam);
				hex.setColor(col);
				hex.setSide(side);
				hex.draw();
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin.get() >> x;
			if (std::cin.fail())
			{
				throw new InputException(BAD_TYPE);
			}
		}
		catch (InputException * error)
		{
			std::cout << error->what() << std::endl;
			if (BAD_TYPE == error->getError())
			{
				std::cin.clear();//switching from fail state.
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');//clearing existing input
			}
		}
		catch (std::exception * e)
		{
			std::cout << e->what() << std::endl;
		}
		catch(...)
		{ 
			printf("caught a bad exception. continuing as usual\n");
		}
	}



		system("pause");
		return 0;
	
}