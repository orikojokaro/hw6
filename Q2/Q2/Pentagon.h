#pragma once
#include "shape.h"
#include "MathUtils.h"
#include "shapeexception.h"
class Pentagon: public Shape
{
	double _side;
public:
	//no pointers, no need for =
	void draw();
	double getArea() const;
	Pentagon(std::string name, double side);
	~Pentagon();
	void setSide(double side) 
	{
		if (side < 0)
		{
			throw new shapeException;
		}
		_side = side;
	}
};

