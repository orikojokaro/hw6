#include "Hexagon.h"



void Hexagon::draw()
{
	std::cout << "Side :" << _side << std::endl <<  getArea() << std::endl;
}

double Hexagon::getArea() const
{
	return MathUtils::CalHexagonArea(_side);
}

Hexagon::Hexagon(std::string name, double side): Shape(name, "Hexagon"), _side(side)
{
}

Hexagon::~Hexagon()
{
}

