#include "MathUtils.h"



MathUtils::MathUtils()
{
}


MathUtils::~MathUtils()
{
}

double MathUtils::CalPentagonArea(const double len)
{
	return 0.25 * (sqrt( 5 * ( 5 + 2 * sqrt(5)))) * (len * len);
}

double MathUtils::CalHexagonArea(const double len)
{
	return (3 * sqrt(3) / 2) *( len * len);
}
