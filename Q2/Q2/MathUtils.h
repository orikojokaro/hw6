#pragma once
#include <math.h>
class MathUtils
{
public://no vars, so no = needed.
	MathUtils();
	~MathUtils();
	static double CalPentagonArea(const double len);
	static double CalHexagonArea(const double len);
};

