#include <iostream>
#include <string>
#define NOT_ALLOWED 8200

//valid is false if no errors rose.
int add(int a, int b, bool & valid) {
	if (NOT_ALLOWED == a+ b)
	{
		valid = false;
		return 0;
	}
	else
	{
		valid = true;
		return (int) a + b;
	}
}

//valid is false if no errors rose.
int  multiply(int a, int b, bool & valid) {
	int sum = 0;
	for(int i = 0; i < b; i++) {
		sum = add(sum, a, valid);
		if (NOT_ALLOWED == sum || NOT_ALLOWED == i)
		{
			valid = false;
			return 0;
		}
	}
	return sum;
}

int pow(int a, int b, bool &valid) {
	int exponent = 1;
	for(int i = 0; i < b; i++) {
		exponent = multiply(exponent, a, valid);
		if(false == valid || NOT_ALLOWED == i || NOT_ALLOWED == exponent){
			valid = false;
			return 0;
		}
	}
	return exponent;
}

int main(void) {
	bool valid = true;
	int res = 0;//the first of each func should fail, the second - not.
	res = add(8100, 100, valid);
	if (valid)
	{
		std::cout << res << std::endl;
	}
	else
	{
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
	}
	res = add(1, 100, valid);
	if (valid)
	{
		std::cout << res << std::endl;
	}
	else
	{
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
	}
	res = multiply(5, 1640, valid);
	if (valid)
	{
		std::cout << res << std::endl;
	}
	else
	{
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
	}
	res = multiply(2, 100, valid);
	if (valid)
	{
		std::cout << res << std::endl;
	}
	else
	{
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
	}
	res = pow(1, 10000, valid);
	if (valid)
	{
		std::cout << res << std::endl;
	}
	else
	{
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
	}
	res = pow(1, 1000, valid);
	if (valid)
	{
		std::cout << res << std::endl;
	}
	else
	{
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
	}
	system("pause");
	return 0;
}