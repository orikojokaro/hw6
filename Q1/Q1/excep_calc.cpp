#include <iostream>
#include <string>
#define NOT_ALLOWED 8200
int add(int a, int b) {
	if (NOT_ALLOWED == a + b)
	{
		throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year"));
		return 0;
	}
	else
	{
		return a + b;
	}	
}

int  multiply(int a, int b) {
	int sum = 0;
	for(int i = 0; i < b; i++) {
		try
		{
			sum = add(sum, a);
			if (NOT_ALLOWED == i || NOT_ALLOWED == sum)
			{
				throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year"));
			}
		}
		catch (std::string error)
		{
			throw(error);
		}
	}
	return sum;
}

int pow(int a, int b) {
	int exponent = 1;
	for(int i = 0; i < b; i++) {
		try
		{
			exponent = multiply(exponent, a);
			if (NOT_ALLOWED == i || NOT_ALLOWED == exponent)
			{
				throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year"));
			}
		}
		catch (std::string error)
		{
			throw(error);
		}
	}
	return exponent;
}

int main(void) {
	try
	{
		std::cout << add(0, 5) << std::endl;
	}
	catch (std::string error)
	{
		std::cout << error << std::endl;
	}
	try
	{
		std::cout << multiply(5, 5) << std::endl;
	}
	catch (std::string error)
	{
		std::cout << error << std::endl;
	}
	try
	{
		std::cout << pow(1, 5) << std::endl;
	}
	catch (std::string error)
	{
		std::cout << error << std::endl;
	}
	system("pause");
	return 0;
}